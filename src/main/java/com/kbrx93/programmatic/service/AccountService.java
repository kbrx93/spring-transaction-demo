package com.kbrx93.programmatic.service;

/**
 * 实现转账业务
 *
 * @author: kbrx93
 */

public interface AccountService {

    /**
     * 转账操作
     * @param out   转出账号
     * @param in    转入账号
     * @param money 金额
     */
    void transfer(String out, String in, int money);
}
