package com.kbrx93.programmatic.service;

import com.kbrx93.programmatic.dao.AccountDAO;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

/**
 * @author: kbrx93
 */
public class AccountServiceImpl implements AccountService {

    private AccountDAO dao;

    public void setDao(AccountDAO dao) {
        this.dao = dao;
    }

    private TransactionTemplate transactionTemplate;

    public void setTransactionTemplate(TransactionTemplate transactionTemplate) {
        this.transactionTemplate = transactionTemplate;
    }

    @Override
    public void transfer(String out, String in, int money) {
        transactionTemplate.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus status) {
                dao.out(out, money);
                int i = 1 / 0;
                dao.in(in, money);
            }
        });
    }
}
