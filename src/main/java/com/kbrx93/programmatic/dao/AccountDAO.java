package com.kbrx93.programmatic.dao;

/**
 * 账户操作DAO类
 *
 * @author: kbrx93
 */

public interface AccountDAO {

    /**
     * 转入操作
     * @param inAccount 转入账户
     * @param money 转入金额
     */
    void in(String inAccount, int money);

    /**
     * 转出操作
     * @param outAccount 转出账户
     * @param money 转出金额
     */
    void out(String outAccount, int money);
}
