package com.kbrx93.programmatic.dao;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

import javax.sql.DataSource;

/**
 * @author: kbrx93
 */
public class AccountDAOImpl extends JdbcDaoSupport implements AccountDAO {

    private DataSource dataSource;

    @Override
    public void in(String inAccount, int money) {
        String sql = "update transfer set money = money + ? WHERE account = ?";
        this.getJdbcTemplate().update(sql, money, inAccount );
    }

    @Override
    public void out(String outAccount, int money) {
        String sql = "update transfer set money = money - ? where account = ?";
        this.getJdbcTemplate().update(sql, money, outAccount);
    }
}
