package com.kbrx93.declarative.proxy.service;

/**
 * @author: kbrx93
 */

public interface AccountService {

    void transfer(String outAccount, String inAccount, int money);
}
