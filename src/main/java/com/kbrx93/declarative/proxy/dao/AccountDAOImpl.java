package com.kbrx93.declarative.proxy.dao;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

/**
 * @author: kbrx93
 */
public class AccountDAOImpl extends JdbcDaoSupport implements AccountDAO {


    @Override
    public void in(String inAccount, int money) {
        String sql = "update transfer set money = money + ? where account = ?";
        this.getJdbcTemplate().update(sql, money, inAccount);
    }

    @Override
    public void out(String outAccount, int money) {
        String sql = "update transfer set money = money - ? where account = ?";
        this.getJdbcTemplate().update(sql, money, outAccount);
    }
}
