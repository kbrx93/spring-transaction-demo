package com.kbrx93.declarative.proxy.dao;

/**
 * @author: kbrx93
 */

public interface AccountDAO {

    /**
     *
     * @param inAccount
     * @param money
     */
    void in(String inAccount, int money);

    /**
     *
     * @param outAccount
     * @param money
     */
    void out(String outAccount, int money);

}
