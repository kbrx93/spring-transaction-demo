package com.kbrx93.declarative.aspectj_xml.service;

import com.kbrx93.declarative.aspectj_xml.dao.AccountDAO;

/**
 * @author: kbrx93
 */
public class AccountServiceImpl implements AccountService {

    private AccountDAO dao;

    public void setDao(AccountDAO dao) {
        this.dao = dao;
    }

    @Override
    public void transfer(String outAccount, String inAccount, int money) {
        dao.out(outAccount, money);
        int i = 1 / 0;
        dao.in(inAccount, money);

    }
}
