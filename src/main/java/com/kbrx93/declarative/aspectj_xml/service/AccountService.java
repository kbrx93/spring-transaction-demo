package com.kbrx93.declarative.aspectj_xml.service;

/**
 * @author: kbrx93
 */

public interface AccountService {

    void transfer(String outAccount, String inAccount, int money);
}
