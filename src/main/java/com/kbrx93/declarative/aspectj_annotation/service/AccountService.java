package com.kbrx93.declarative.aspectj_annotation.service;

/**
 * @author: kbrx93
 */

public interface AccountService {

    void transfer(String outAccount, String inAccount, int money);
}
