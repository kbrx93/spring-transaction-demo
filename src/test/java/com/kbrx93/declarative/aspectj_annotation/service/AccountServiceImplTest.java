package com.kbrx93.declarative.aspectj_annotation.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author: kbrx93
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:declarative-aspectj-annotation.xml")
public class AccountServiceImplTest {

    @Autowired
    private AccountService service;

    @Test
    public void transfer() {
        service.transfer("A", "B", 200);
    }
}