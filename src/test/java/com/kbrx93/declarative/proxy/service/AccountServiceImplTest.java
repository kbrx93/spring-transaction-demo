package com.kbrx93.declarative.proxy.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

/**
 * @author: kbrx93
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:declarative-proxy.xml")
public class AccountServiceImplTest {

    @Resource(name = "serviceProxy")
    private AccountService accountService;

    @Test
    public void transfer() {
        accountService.transfer("A", "B", 200);
    }
}