package com.kbrx93.declarative.aspectj_xml.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author: kbrx93
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:declarative-aspectj-xml.xml")
public class AccountServiceImplTest {

    @Autowired
    private AccountService accountService;

    @Test
    public void transfer() {
        accountService.transfer("A", "B", 200);
    }
}